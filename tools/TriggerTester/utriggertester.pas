unit uTriggerTester;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, strutils;

type

  { TFormTriggerTester }

  TFormTriggerTester = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Reset: TButton;
    Door3: TCheckBox;
    Door2: TCheckBox;
    Door1: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ResetClick(Sender: TObject);
  private

  public

  end;

var
  FormTriggerTester: TFormTriggerTester;

implementation

{$R *.lfm}

{ TFormTriggerTester }

procedure TFormTriggerTester.Button1Click(Sender: TObject);
var S : String;
begin
Rever
  Door1.Checked := true;
  Door2.Checked := false;
//  Door3.Checked := true;
end;

procedure TFormTriggerTester.Button2Click(Sender: TObject);
begin
//  Door1.Checked := false;
  Door2.Checked := true;
  Door3.Checked := false;
end;

procedure TFormTriggerTester.Button3Click(Sender: TObject);
begin
  Door3.Checked := true;
end;

procedure TFormTriggerTester.ResetClick(Sender: TObject);
begin
  Door1.Checked := false;
  Door2.Checked := true;
  Door3.Checked := false;
end;

end.

